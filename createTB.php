<?php
$severname = "localhost";
$username = "root";
$password = "";
$dbname = "bookstore_db";

//Create connection
$conn = new mysqli($severname, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

//Create Database
/* $sql = "CREATE TABLE staffs (
    id INT(3) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    firstname VARCHAR(30) NOT NULL,
    lastname VARCHAR(30) NOT NULL,
    age int(3) UNSIGNED NOT NULL,
    reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
)"; */

/* $sql = "CREATE TABLE books (
    ISBN INT(13) UNSIGNED PRIMARY KEY,
    bookname VARCHAR(50) NOT NULL,
    price int(5) UNSIGNED NOT NULL,
    reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
)"; */

$sql = "CREATE TABLE books_purchase (
    pur_id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    ISBN INT(13) UNSIGNED NOT NULL,
    staff_id INT (3) UNSIGNED NOT NULL,
    price int(5) UNSIGNED NOT NULL,
    amount int(3) UNSIGNED NOT NULL,
    pur_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
)";

if($conn->query($sql) === TRUE) {
    echo "Table \"books_purchase\" created successfully";
} else {
    echo "Error creating table: " . $conn->error;
}

$conn->close();
?>