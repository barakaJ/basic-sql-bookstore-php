<?php
$severname = "localhost";
$username = "root";
$password = "";
$dbname = "bookstore_db";

//Create connection
$conn = new mysqli($severname, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT SUM(amount) FROM books_purchase";
$result = $conn->query($sql);

if($result->num_rows > 0) {
    $sum = $result->fetch_array();
    echo "Total purchased book amount is " . $sum[0] . ".";
} else {
    echo "Error: " . $conn->error;
}

$conn->close();
?>