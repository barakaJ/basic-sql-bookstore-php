<!DOCTYPE html>
<html>
<head>
<style>
    table, th, td {
        border: 1px solid black;
    }

    th {
        background-color: #98bfe3;
    }
</style>
</head>

<body>

<?php
$severname = "localhost";
$username = "root";
$password = "";
$dbname = "bookstore_db";

//Create connection
$conn = new mysqli($severname, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT DISTINCT books_purchase.ISBN , books.bookname FROM books_purchase JOIN books ON books_purchase.ISBN = books.ISBN";
$result = $conn->query($sql);

if($result->num_rows > 0) {
    echo "<h3>List of purchased books</h3>";
    echo "<table><tr><th>ISBN</th><th>Title</th></tr>";
    while($row = $result->fetch_assoc()) {
        echo "<tr><td>" . $row["ISBN"] . "</td><td>" . $row["bookname"] . "</td></tr>";
    }
    echo "</table>";
} else {
    echo "Error: " . $conn->error;
}

$conn->close();
?>

</body>
</html>