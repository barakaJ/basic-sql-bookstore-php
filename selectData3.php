<!DOCTYPE html>
<html>
<head>
<style>
    table, th, td {
        border: 1px solid black;
    }

    th {
        background-color: #98bfe3;
    }
</style>
</head>

<body>

<?php
$severname = "localhost";
$username = "root";
$password = "";
$dbname = "bookstore_db";

//Create connection
$conn = new mysqli($severname, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

$sql_s = "SELECT firstname, age FROM staffs ORDER BY id";
$result_s = $conn->query($sql_s);

if($result_s->num_rows > 0) {
    echo "<table><tr><th>First name</th><th>Age</th></tr>";
    //display data in loop
    while($row = $result_s->fetch_assoc()) {
        echo "<tr><td>" . $row["firstname"] . "</td><td>" . $row["age"] . "</td></tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
}

$conn->close();
?>

</body>
</html>