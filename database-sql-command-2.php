<!DOCTYPE html>
<html>
<head>
<style>
    table, th, td {
        border: 1px solid black;
    }

    th {
        background-color: #98bfe3;
    }
    table {
        display: table-cell;
    }
</style>
</head>

<body>

<?php
$severname = "localhost";
$username = "root";
$password = "";
$dbname = "bookstore_db";

//Create connection
$conn = new mysqli($severname, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

/*-------------------Display all books-------------------------*/
$sql_b = "SELECT * FROM books ORDER BY reg_date";
$result_b = $conn->query($sql_b);

if($result_b->num_rows > 0) {
    echo "<table><tr><th colspan=4>All Book</th></tr>";
    echo "<tr><th>ISBN</th><th>Title</th><th>Price</th><th>Reg-Date</th></tr>";
    //display data in loop
    while($row = $result_b->fetch_assoc()) {
        echo "<tr><td>" . $row["ISBN"] . "</td><td>" . $row["bookname"] . "</td><td>" . $row["price"] . "</td><td>" . $row["reg_date"] . "</td></tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
}


/*-------------------Display purchased books-------------------------*/
$sql_p = "SELECT * FROM books_purchase";
$result_p = $conn->query($sql_p);

if($result_p->num_rows > 0) {
    echo "<table><tr><th colspan=2>Purchased List</th></tr>";
    echo "<tr><th>ISBN</th><th>Amount</th></tr>";
    //display data in loop
    while($row = $result_p->fetch_assoc()) {
        echo "<tr><td>" . $row["ISBN"] . "</td><td>" . $row["amount"] . "</td></tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
}


/*-------------------Find books with 'mar' keyword-------------------------*/
$sql = "SELECT * FROM books WHERE bookname LIKE '%mar%'";
$result = $conn->query($sql);

if($result->num_rows > 0) {
    echo "<h3>Search result of \"mar\"</h3>";
    echo "<table><tr><th>Title</th></tr>";
    while($row = $result->fetch_assoc()) {
        echo "<tr><td>" . $row["bookname"] . "</td></tr>";
    }
    echo "</table>";
} else {
    echo "0 results";
}


/*-------------------Fins only 2 books with 'o' keyword-------------------------*/
$sql = "SELECT * FROM books WHERE bookname LIKE '%o%' LIMIT 2";
$result = $conn->query($sql);

if($result->num_rows > 0) {

    echo "<h3>'2' Search results of \"o\"</h3>";
    echo "<table><tr><th>Title</th></tr>";

    while($row = $result->fetch_assoc()) {
        echo "<tr><td>" . $row["bookname"] . "</td></tr>";
    }

    echo "</table>";
} else {
    echo "0 results";
}


/*-------------------Display amount purchased books-------------------------*/
$sql = "SELECT SUM(amount) FROM books_purchase";
$result = $conn->query($sql);

if($result->num_rows > 0) {
    $sum = $result->fetch_array();
    echo "<br>Total purchased book amount is <strong>" . $sum[0] . "</strong>.";
} else {
    echo "Error: " . $conn->error;
}


/*-------------------Calculate total income-------------------------*/
$sql = "SELECT SUM(books_purchase.amount * books.price) FROM books_purchase JOIN books ON books_purchase.ISBN = books.ISBN";
$result = $conn->query($sql);

if($result->num_rows > 0) {
    $sum = $result->fetch_array();
    echo "<br>Total income amount is <strong>" . $sum[0] . "</strong>.";
} else {
    echo "Error: " . $conn->error;
}

$conn->close();
?>

</body>
</html>