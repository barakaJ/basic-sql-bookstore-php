<!DOCTYPE html>
<html>
<head>
<style>
    table, th, td {
        border: 1px solid black;
    }

    th {
        background-color: #98bfe3;
    }
</style>
</head>

<body>

<?php
$severname = "localhost";
$username = "root";
$password = "";
$dbname = "bookstore_db";

//Create connection
$conn = new mysqli($severname, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM books WHERE bookname LIKE '%mar%'";
$result = $conn->query($sql);

if($result->num_rows > 0) {

    echo "<h3>Search result of \"mar\".";
    echo "<table><tr><th>Title</th></tr>";

    while($row = $result->fetch_assoc()) {
        echo "<tr><td>" . $row["bookname"] . "</td></tr>";
    }

    echo "</table>";
} else {
    echo "0 results";
}


$conn->close();
?>

</body>
</html>