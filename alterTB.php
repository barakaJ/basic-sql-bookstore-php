<?php
$severname = "localhost";
$username = "root";
$password = "";
$dbname = "bookstore_db";

//Create connection
$conn = new mysqli($severname, $username, $password, $dbname);
//Check connection
if ($conn->connect_error) {
    exit("Connection failed: " . $conn->connect_error);
}

$sql = "ALTER TABLE books_purchase
DROP COLUMN price";

if ($conn->query($sql) === TRUE) {
    echo "Modify column successfully";
} else {
    echo "Error: " . $conn->error;
}

$conn->close();
?>